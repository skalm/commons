package io.skalm.commons.json.jackson;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import java.io.IOException;

/**
 * Json handling helper based on Jackson v2.0+
 * <ul>
 * <li><b>getJsonTreeFromString</b> - Get JsonNode tree from String</li>
 * <li><b>getInt/getString/getJsonNode/...</b> - Get value is key exists and value is expected type</li>
 * </ul>
 */
public class JacksonJsonHelper {

    /**
     * ObjectMapper used in methods
     */
    private static final ObjectMapper mapper = new ObjectMapper();

    /**
     * JacksonJsonHelper instances should NOT be constructed in standard programming.
     */
    private JacksonJsonHelper() {
        // Hide public constructor
    }

    /**
     * Read Json tree from input string
     *
     * <pre>
     *      JacksonJsonHelper.readJsonTreeFromString(null)                -> java.lang.IllegalArgumentException
     *      JacksonJsonHelper.readJsonTreeFromString("str")               -> com.fasterxml.jackson.core.JsonParseException
     *      JacksonJsonHelper.readJsonTreeFromString("")                  = com.fasterxml.jackson.databind.node.MissingNode instance
     *      JacksonJsonHelper.readJsonTreeFromString(" ")                 = com.fasterxml.jackson.databind.node.MissingNode instance
     *      JacksonJsonHelper.readJsonTreeFromString("[]"])               = []
     *      JacksonJsonHelper.readJsonTreeFromString("[3,4]"])            = [3,4]
     *      JacksonJsonHelper.readJsonTreeFromString("[{"key":4}]")       = [{"key":4}]
     *      JacksonJsonHelper.readJsonTreeFromString("{}"])               = {}
     *      JacksonJsonHelper.readJsonTreeFromString("{"key":[]}")        = {"key":[]}
     *      JacksonJsonHelper.readJsonTreeFromString("[4,true,"str"]"])   = [4,true,"str"]
     * </pre>
     *
     * @param stringTree Input string to read json from
     * @return JsonNode read from input string
     * @throws IOException if json can't be read from input string
     */
    public static JsonNode readJsonTreeFromString(final String stringTree) throws IOException {
        return mapper.readTree(stringTree);
    }

    /**
     * Check if input JsonNode is null or if its type is JsonNodeType.NULL
     *
     * <pre>
     *     isNodeNull(null)                                                         = true
     *     isNodeNull(com.fasterxml.jackson.databind.node.NullNode.getInstance())   = true
     *     isNodeNull({})                                                           = false
     *     isNodeNull([])                                                           = false
     * </pre>
     */
    private static boolean isNodeNull(final JsonNode node) {
        return node == null || node.isNull();
    }

    /**
     * Get value mapped to key in node if value is an integer
     *
     * <pre>
     *     JacksonJsonHelper.getInt(null,"key")                = null
     *     JacksonJsonHelper.getInt([],"key")                  = null
     *     JacksonJsonHelper.getInt({},"key")                  = null
     *     JacksonJsonHelper.getInt({"key":"NULL"},"key")      = null
     *     JacksonJsonHelper.getInt({"key":"null"},"key")      = null
     *     JacksonJsonHelper.getInt({"key":{}},"key")          = null
     *     JacksonJsonHelper.getInt({"key":[]},"key")          = null
     *     JacksonJsonHelper.getInt({"key":""},"key")          = null
     *     JacksonJsonHelper.getInt({"key":0.0},"key")         = null
     *     JacksonJsonHelper.getInt({"key":"5"},"key")         = null
     *     JacksonJsonHelper.getInt({"key":5},"key")           = 5
     *     JacksonJsonHelper.getInt({"key":-5},"key")          = -5
     * </pre>
     *
     * @param node JsonNode to read value from
     * @param key  String key to read value from
     * @return <ul>
     * <li>null if it can't read key from node</li>
     * <li>null if value is not an integer</li>
     * <li>value otherwise</li>
     * </ul>
     */
    public static Integer getInt(final JsonNode node, final String key) {
        if (isNodeNull(node)) {
            return null;
        } else {
            final JsonNode value = node.get(key);
            if (value == null || !value.isInt()) {
                return null;
            } else {
                return value.intValue();
            }
        }
    }

    /**
     * Get value mapped to key in node if value is a string
     *
     * <pre>
     *     JacksonJsonHelper.getString(null,"key")                = null
     *     JacksonJsonHelper.getString([],"key")                  = null
     *     JacksonJsonHelper.getString({},"key")                  = null
     *     JacksonJsonHelper.getString({"key":{}},"key")          = null
     *     JacksonJsonHelper.getString({"key":[]},"key")          = null
     *     JacksonJsonHelper.getString({"key":5},"key")           = null
     *     JacksonJsonHelper.getString({"key":""},"key")          = ""
     *     JacksonJsonHelper.getString({"key":" "},"key")         = " "
     *     JacksonJsonHelper.getString({"key":"5"},"key")         = "5"
     *     JacksonJsonHelper.getString({"key":"NULL"},"key")      = "NULL"
     *     JacksonJsonHelper.getString({"key":"null"},"key")      = "null"
     *     JacksonJsonHelper.getString({"key":"{}"},"key")        = "{}"
     *     JacksonJsonHelper.getString({"key":"str"},"key")       = "str"
     * </pre>
     *
     * @param node JsonNode to read value from
     * @param key  String key to read value from
     * @return <ul>
     * <li>null if it can't read key from node</li>
     * <li>null if value is not a string</li>
     * <li>value otherwise</li>
     * </ul>
     */
    public static String getString(final JsonNode node, final String key) {
        if (isNodeNull(node)) {
            return null;
        } else {
            final JsonNode value = node.get(key);
            if (value == null || !value.isTextual()) {
                return null;
            } else {
                return value.textValue();
            }
        }
    }

    /**
     * Get value mapped to key in node if value is a string
     *
     * <pre>
     *     JacksonJsonHelper.getJsonNode(null,"key")                    = null
     *     JacksonJsonHelper.getJsonNode([],"key")                      = null
     *     JacksonJsonHelper.getJsonNode({},"key")                      = null
     *     JacksonJsonHelper.getJsonNode({"key":[]},"key")              = null
     *     JacksonJsonHelper.getJsonNode({"key":5},"key")               = null
     *     JacksonJsonHelper.getJsonNode({"key":""},"key")              = null
     *     JacksonJsonHelper.getJsonNode({"key":"{}"},"key")            = null
     *     JacksonJsonHelper.getJsonNode({"key":{}},"key")              = {}
     *     JacksonJsonHelper.getJsonNode({"key":{"key":"str"}},"key")   = {"key":"str"}
     * </pre>
     *
     * @param node JsonNode to read value from
     * @param key  String key to read value from
     * @return <ul>
     * <li>null if it can't read key from node</li>
     * <li>null if value is not a json node</li>
     * <li>value otherwise</li>
     * </ul>
     */
    public static JsonNode getJsonNode(final JsonNode node, final String key) {
        if (isNodeNull(node)) {
            return null;
        } else {
            final JsonNode value = node.get(key);
            if (value == null || value.getNodeType() != JsonNodeType.OBJECT) {
                return null;
            } else {
                return value;
            }
        }
    }
}
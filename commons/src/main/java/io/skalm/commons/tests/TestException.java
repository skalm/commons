package io.skalm.commons.tests;

public class TestException extends Exception {
    public TestException(final Exception e) {
        super(e);
    }
}

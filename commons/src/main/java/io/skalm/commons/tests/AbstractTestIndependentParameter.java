package io.skalm.commons.tests;

import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.provider.Arguments;

public abstract class AbstractTestIndependentParameter {

    /**
     * Test asserting that all planned values for tested argument are tested
     */
    @DisplayName("Assert that all planned values for this argument are tested")
    @Test
    void assertTestArgumentsProviderProvidesAllValuesForTestedParameter() {
        Assertions.assertThat(getTestArguments().map(arguments -> arguments.get()[0]).collect(Collectors.toSet()))
                .containsExactlyInAnyOrderElementsOf(Arrays.asList(getTestArgumentEnum().getEnumConstants()));
    }

    /**
     * Test asserting that tested method return expected result or throws expected exception for tested argument
     */
    public void test(Object arg, Object expectedResult) {
        try {
            Assertions.assertThat(callTestedMethod(arg)).isEqualTo(expectedResult);
        } catch (final TestException e) {
            Assertions.assertThat(e.getCause()).isInstanceOf((Class<?>) expectedResult);
        }
    }

    /**
     * Modify the stream provided by getTestArguments() to get value from the TestArgumentEnum providing arguments
     */
    protected Stream<Arguments> getTestArgumentsProvider() {
        return this.getTestArguments()
                .map(arg -> Arguments.of(((TestArgumentEnum) arg.get()[0]).getValue(), arg.get()[1]));
    }

    /**
     * Method made to be overridden to call test(Object arg, Object expectedResult) with required annotations (the name
     * of the @ParameterizedTest should be adapted):
     * <ul>
     *     <li>@DisplayName("Call method with relevant values for this parameter")</li>
     *     <li>@ParameterizedTest(name = "{index} => Test method(args..., {0}, args..."), expecting {1}")</li>
     *     <li>@MethodSource("getTestArgumentsProvider")</li>
     * </ul>
     */
    @DisplayName("Call method with relevant values for this parameter")
    @Test
    protected abstract void callTest(Object arg, Object expectedResult);

    /**
     * Method made to be overridden to return a stream of Argument(TestArgumentEnum testedValue, Object expectedReturn)
     */
    protected abstract Stream<Arguments> getTestArguments();

    /**
     * Method made to be overridden to call tested method
     */
    protected abstract Object callTestedMethod(final Object arg)
            throws TestException;

    /**
     * Method made to be overridden to return the TestArgumentEnum containing values to test
     */
    protected abstract Class<? extends TestArgumentEnum> getTestArgumentEnum();
}

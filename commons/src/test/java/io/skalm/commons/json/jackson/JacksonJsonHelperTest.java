package io.skalm.commons.json.jackson;


import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import com.fasterxml.jackson.databind.node.MissingNode;
import io.skalm.commons.tests.AbstractTestIndependentParameter;
import io.skalm.commons.tests.TestArgumentEnum;
import io.skalm.commons.tests.TestException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.stream.Stream;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

@DisplayName("Test JacksonJsonHelper")
class JacksonJsonHelperTest {

    private static final ObjectMapper mapper = new ObjectMapper();

    private enum JsonNodeArgument implements TestArgumentEnum {
        Null(JsonNodeArgumentConstants.NULL),
        NullNode(JsonNodeArgumentConstants.NULL_NODE),
        EmptyNode(JsonNodeArgumentConstants.EMPTY_NODE),
        ArrayNode(JsonNodeArgumentConstants.ARRAY_NODE),
        EmptyValue(JsonNodeArgumentConstants.EMPTY_VALUE),
        EmptyNodeValue(JsonNodeArgumentConstants.EMPTY_NODE_VALUE),
        ArrayNodeValue(JsonNodeArgumentConstants.ARRAY_NODE_VALUE),
        NullNodeValue(JsonNodeArgumentConstants.NULL_NODE_VALUE),
        ZeroIntValue(JsonNodeArgumentConstants.ZERO_INT_VALUE),
        IntValue(JsonNodeArgumentConstants.INT_VALUE),
        NegativeIntValue(JsonNodeArgumentConstants.NEGATIVE_INT_VALUE),
        BooleanTrueValue(JsonNodeArgumentConstants.BOOLEAN_TRUE_VALUE),
        BooleanFalseValue(JsonNodeArgumentConstants.BOOLEAN_FALSE_VALUE),
        BooleanTrueIntValue(JsonNodeArgumentConstants.BOOLEAN_TRUE_INT_VALUE),
        ZeroDoubleValue(JsonNodeArgumentConstants.ZERO_DOUBLE_VALUE),
        DoubleValue(JsonNodeArgumentConstants.DOUBLE_VALUE),
        NegativeDoubleValue(JsonNodeArgumentConstants.NEGATIVE_DOUBLE_VALUE),
        IntDoubledValue(JsonNodeArgumentConstants.INT_DOUBLED_VALUE),
        NegativeIntDoubledValue(JsonNodeArgumentConstants.NEGATIVE_INT_DOUBLED_VALUE),
        StringNullValue(JsonNodeArgumentConstants.STRING_NULL_VALUE),
        StringJsonNodeTypeNullValue(JsonNodeArgumentConstants.STRING_JSON_NODE_TYPE_NULL_VALUE),
        StringEmptyNodeValue(JsonNodeArgumentConstants.STRING_EMPTY_NODE_VALUE),
        StringArrayNodeValue(JsonNodeArgumentConstants.STRING_ARRAY_NODE_VALUE),
        StringZeroIntValue(JsonNodeArgumentConstants.STRING_ZERO_INT_VALUE),
        StringIntValue(JsonNodeArgumentConstants.STRING_INT_VALUE),
        StringNegativeIntValue(JsonNodeArgumentConstants.STRING_NEGATIVE_INT_VALUE),
        StringBooleanTrueValue(JsonNodeArgumentConstants.STRING_BOOLEAN_TRUE_VALUE),
        StringBooleanFalseValue(JsonNodeArgumentConstants.STRING_BOOLEAN_FALSE_VALUE),
        StringBooleanTrueIntValue(JsonNodeArgumentConstants.STRING_BOOLEAN_TRUE_INT_VALUE),
        StringZeroDoubleValue(JsonNodeArgumentConstants.STRING_ZERO_DOUBLE_VALUE),
        StringDoubleValue(JsonNodeArgumentConstants.STRING_DOUBLE_VALUE),
        StringNegativeDoubleValue(JsonNodeArgumentConstants.STRING_NEGATIVE_DOUBLE_VALUE),
        StringIntDoubledValue(JsonNodeArgumentConstants.STRING_INT_DOUBLED_VALUE),
        StringNegativeIntDoubledValue(JsonNodeArgumentConstants.STRING_NEGATIVE_INT_DOUBLED_VALUE),
        StringBlankValue(JsonNodeArgumentConstants.STRING_BLANK_VALUE),
        StringValue(JsonNodeArgumentConstants.STRING_VALUE),
        NestedNodeValue(JsonNodeArgumentConstants.NESTED_NODE_VALUE);

        private final JsonNode jsonNode;

        JsonNodeArgument(JsonNode jsonNode) {
            this.jsonNode = jsonNode;
        }

        @Override
        public Object getValue() {
            return this.jsonNode;
        }

        private static class JsonNodeArgumentConstants {

            private static final String KEY = "key";

            private static final JsonNode NULL = null;
            private static final JsonNode NULL_NODE = com.fasterxml.jackson.databind.node.NullNode.getInstance();
            private static final JsonNode EMPTY_NODE = mapper.createObjectNode();
            private static final JsonNode ARRAY_NODE = mapper.createArrayNode();
            private static final String EMPTY = "";
            private static final JsonNode EMPTY_VALUE = mapper.createObjectNode().put(KEY, EMPTY);
            private static final JsonNode EMPTY_NODE_VALUE =
                    mapper.createObjectNode().set(KEY, mapper.createObjectNode());
            private static final JsonNode ARRAY_NODE_VALUE =
                    mapper.createObjectNode().set(KEY, mapper.createArrayNode());
            private static final JsonNode NULL_NODE_VALUE =
                    mapper.createObjectNode().set(KEY, com.fasterxml.jackson.databind.node.NullNode.getInstance());
            private static final int ZERO_INT = 0;
            private static final JsonNode ZERO_INT_VALUE = mapper.createObjectNode().put(KEY, ZERO_INT);
            private static final int INT = 5;
            private static final JsonNode INT_VALUE = mapper.createObjectNode().put(KEY, INT);
            private static final int NEGATIVE_INT = -5;
            private static final JsonNode NEGATIVE_INT_VALUE = mapper.createObjectNode().put(KEY, NEGATIVE_INT);
            private static final JsonNode BOOLEAN_TRUE_VALUE = mapper.createObjectNode().put(KEY, Boolean.TRUE);
            private static final JsonNode BOOLEAN_FALSE_VALUE = mapper.createObjectNode().put(KEY, Boolean.FALSE);
            private static final int BOOLEAN_TRUE_INT = 1;
            private static final JsonNode BOOLEAN_TRUE_INT_VALUE = mapper.createObjectNode().put(KEY, BOOLEAN_TRUE_INT);
            private static final double ZERO_DOUBLE = 0.0;
            private static final JsonNode ZERO_DOUBLE_VALUE = mapper.createObjectNode().put(KEY, ZERO_DOUBLE);
            private static final double DOUBLE = 5.1;
            private static final JsonNode DOUBLE_VALUE = mapper.createObjectNode().put(KEY, DOUBLE);
            private static final double NEGATIVE_DOUBLE = -5.1;
            private static final JsonNode NEGATIVE_DOUBLE_VALUE = mapper.createObjectNode().put(KEY, NEGATIVE_DOUBLE);
            private static final double INT_DOUBLED = 5.0;
            private static final JsonNode INT_DOUBLED_VALUE = mapper.createObjectNode().put(KEY, INT_DOUBLED);
            private static final double NEGATIVE_INT_DOUBLED = -5.0;
            private static final JsonNode NEGATIVE_INT_DOUBLED_VALUE =
                    mapper.createObjectNode().put(KEY, NEGATIVE_INT_DOUBLED);
            private static final String STRING_NULL = String.valueOf(NULL);
            private static final JsonNode STRING_NULL_VALUE = mapper.createObjectNode().put(KEY, STRING_NULL);
            private static final String STRING_JSON_NODE_TYPE_NULL = String.valueOf(JsonNodeType.NULL);
            private static final JsonNode STRING_JSON_NODE_TYPE_NULL_VALUE =
                    mapper.createObjectNode().put(KEY, STRING_JSON_NODE_TYPE_NULL);
            private static final String STRING_EMPTY_NODE = mapper.createObjectNode().toString();
            private static final JsonNode STRING_EMPTY_NODE_VALUE =
                    mapper.createObjectNode().put(KEY, STRING_EMPTY_NODE);
            private static final String STRING_ARRAY_NODE = mapper.createArrayNode().toString();
            private static final JsonNode STRING_ARRAY_NODE_VALUE =
                    mapper.createObjectNode().put(KEY, STRING_ARRAY_NODE);
            private static final String STRING_ZERO_INT = String.valueOf(ZERO_INT);
            private static final JsonNode STRING_ZERO_INT_VALUE = mapper.createObjectNode().put(KEY, STRING_ZERO_INT);
            private static final String STRING_INT = String.valueOf(INT);
            private static final JsonNode STRING_INT_VALUE = mapper.createObjectNode().put(KEY, STRING_INT);
            private static final String STRING_NEGATIVE_INT = String.valueOf(NEGATIVE_INT);
            private static final JsonNode STRING_NEGATIVE_INT_VALUE =
                    mapper.createObjectNode().put(KEY, STRING_NEGATIVE_INT);
            private static final String STRING_BOOLEAN_TRUE = String.valueOf(Boolean.TRUE);
            private static final JsonNode STRING_BOOLEAN_TRUE_VALUE =
                    mapper.createObjectNode().put(KEY, STRING_BOOLEAN_TRUE);
            private static final String STRING_BOOLEAN_FALSE = String.valueOf(Boolean.FALSE);
            private static final JsonNode STRING_BOOLEAN_FALSE_VALUE =
                    mapper.createObjectNode().put(KEY, STRING_BOOLEAN_FALSE);
            private static final String STRING_BOOLEAN_TRUE_INT = String.valueOf(BOOLEAN_TRUE_INT);
            private static final JsonNode STRING_BOOLEAN_TRUE_INT_VALUE =
                    mapper.createObjectNode().put(KEY, STRING_BOOLEAN_TRUE_INT);
            private static final String STRING_ZERO_DOUBLE = String.valueOf(ZERO_DOUBLE);
            private static final JsonNode STRING_ZERO_DOUBLE_VALUE =
                    mapper.createObjectNode().put(KEY, STRING_ZERO_DOUBLE);
            private static final String STRING_DOUBLE = String.valueOf(DOUBLE);
            private static final JsonNode STRING_DOUBLE_VALUE = mapper.createObjectNode().put(KEY, STRING_DOUBLE);
            private static final String STRING_NEGATIVE_DOUBLE = String.valueOf(NEGATIVE_DOUBLE);
            private static final JsonNode STRING_NEGATIVE_DOUBLE_VALUE =
                    mapper.createObjectNode().put(KEY, STRING_NEGATIVE_DOUBLE);
            private static final String STRING_INT_DOUBLED = String.valueOf(INT_DOUBLED);
            private static final JsonNode STRING_INT_DOUBLED_VALUE =
                    mapper.createObjectNode().put(KEY, STRING_INT_DOUBLED);
            private static final String STRING_NEGATIVE_INT_DOUBLED = String.valueOf(NEGATIVE_INT_DOUBLED);
            private static final JsonNode STRING_NEGATIVE_INT_DOUBLED_VALUE =
                    mapper.createObjectNode().put(KEY, STRING_NEGATIVE_INT_DOUBLED);
            private static final String STRING_BLANK = " ";
            private static final JsonNode STRING_BLANK_VALUE = mapper.createObjectNode().put(KEY, STRING_BLANK);
            private static final String STRING = "str";
            private static final JsonNode STRING_VALUE = mapper.createObjectNode().put(KEY, STRING);
            private static final JsonNode NESTED_NODE_VALUE = mapper.createObjectNode().set(KEY, STRING_VALUE);
        }
    }

    private enum KeyArgument implements TestArgumentEnum {
        Null(KeyArgumentConstants.NULL),
        Empty(KeyArgumentConstants.EMPTY),
        Blank(KeyArgumentConstants.BLANK),
        String(KeyArgumentConstants.STRING);

        private final String key;

        KeyArgument(String key) {
            this.key = key;
        }

        @Override
        public Object getValue() {
            return key;
        }

        private static class KeyArgumentConstants {

            private static final String NULL = null;
            private static final String EMPTY = "";
            private static final String BLANK = " ";
            private static final String STRING = "key";

            private static final String STRING_VALUE = "value";
            private static final JsonNode NODE_WITH_STRING_VALUE = mapper.createObjectNode().put(STRING, STRING_VALUE);
            private static final int INT_VALUE = 4;
            private static final JsonNode NODE_WITH_INT_VALUE = mapper.createObjectNode().put(STRING, INT_VALUE);
            private static final JsonNode NODE_VALUE = mapper.createObjectNode().put(STRING, INT_VALUE);
            private static final JsonNode NODE_WITH_NODE_VALUE = mapper.createObjectNode().set(STRING, NODE_VALUE);
        }
    }

    private enum StringJsonTreeArgument implements TestArgumentEnum {
        Null(StringJsonTreeArgumentConstants.NULL),
        Empty(StringJsonTreeArgumentConstants.EMPTY),
        Blank(StringJsonTreeArgumentConstants.BLANK),
        String(StringJsonTreeArgumentConstants.STRING),
        EmptyArray(StringJsonTreeArgumentConstants.EMPTY_ARRAY),
        IntArray(StringJsonTreeArgumentConstants.INT_ARRAY),
        NodeArray(StringJsonTreeArgumentConstants.NODE_ARRAY),
        EmptyNode(StringJsonTreeArgumentConstants.EMPTY_NODE),
        NodeWithArray(StringJsonTreeArgumentConstants.NODE_WITH_ARRAY),
        ArrayWithDifferentTypes(StringJsonTreeArgumentConstants.ARRAY_WITH_DIFFERENT_TYPES);

        private final String stringJsonTree;

        StringJsonTreeArgument(String stringJsonTree) {
            this.stringJsonTree = stringJsonTree;
        }

        @Override
        public Object getValue() {
            return this.stringJsonTree;
        }

        private static class StringJsonTreeArgumentConstants {
            private static final String KEY = "key";

            private static final String NULL = null;
            private static final String EMPTY = "";
            private static final String BLANK = " ";
            private static final String STRING = "str";
            private static final JsonNode EMPTY_ARRAY_NODE = mapper.createArrayNode();
            private static final String EMPTY_ARRAY = EMPTY_ARRAY_NODE.toString();
            private static final JsonNode INT_ARRAY_NODE = mapper.createArrayNode().add(3).add(4);
            private static final String INT_ARRAY = INT_ARRAY_NODE.toString();
            private static final JsonNode NODE_ARRAY_NODE =
                    mapper.createArrayNode().add(mapper.createObjectNode().put(KEY, 4));
            private static final String NODE_ARRAY = NODE_ARRAY_NODE.toString();
            private static final JsonNode EMPTY_NODE_NODE = mapper.createObjectNode();
            private static final String EMPTY_NODE = EMPTY_NODE_NODE.toString();
            private static final JsonNode NODE_WITH_ARRAY_NODE =
                    mapper.createObjectNode().set(KEY, mapper.createArrayNode());
            private static final String NODE_WITH_ARRAY = NODE_WITH_ARRAY_NODE.toString();
            private static final JsonNode ARRAY_WITH_DIFFERENT_TYPES_NODE =
                    mapper.createArrayNode().add(4).add(true).add("str");
            private static final String ARRAY_WITH_DIFFERENT_TYPES = ARRAY_WITH_DIFFERENT_TYPES_NODE.toString();
        }
    }

    @Nested
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    @DisplayName("Test 'getInt(JsonNode node, String key)' method")
    class GetIntTest {

        @Nested
        @TestInstance(TestInstance.Lifecycle.PER_CLASS)
        @DisplayName("Test 'node' parameter in 'getInt(JsonNode node, String key)' method")
        class TestNodeParameter extends AbstractTestIndependentParameter {

            @Override
            protected Object callTestedMethod(final Object arg) {
                return JacksonJsonHelper.getInt((JsonNode) arg, JsonNodeArgument.JsonNodeArgumentConstants.KEY);
            }

            @Override
            protected Class<? extends TestArgumentEnum> getTestArgumentEnum() {
                return JsonNodeArgument.class;
            }

            @Override
            @DisplayName("Call method with relevant values for this parameter")
            @ParameterizedTest(name = "{index} => Test getInt({0}, \"key\"), expecting {1}")
            @MethodSource("getTestArgumentsProvider")
            protected void callTest(Object arg, Object expectedResult) {
                super.test(arg, expectedResult);
            }

            @Override
            protected Stream<Arguments> getTestArguments() {
                return Stream.of(Arguments.of(JsonNodeArgument.Null, null),
                        Arguments.of(JsonNodeArgument.NullNode, null),
                        Arguments.of(JsonNodeArgument.EmptyNode, null),
                        Arguments.of(JsonNodeArgument.ArrayNode, null),
                        Arguments.of(JsonNodeArgument.StringJsonNodeTypeNullValue, null),
                        Arguments.of(JsonNodeArgument.EmptyValue, null),
                        Arguments.of(JsonNodeArgument.EmptyNodeValue, null),
                        Arguments.of(JsonNodeArgument.ArrayNodeValue, null),
                        Arguments.of(JsonNodeArgument.NullNodeValue, null),
                        Arguments.of(JsonNodeArgument.ZeroIntValue,
                                JsonNodeArgument.JsonNodeArgumentConstants.ZERO_INT),
                        Arguments.of(JsonNodeArgument.IntValue, JsonNodeArgument.JsonNodeArgumentConstants.INT),
                        Arguments.of(JsonNodeArgument.NegativeIntValue,
                                JsonNodeArgument.JsonNodeArgumentConstants.NEGATIVE_INT),
                        Arguments.of(JsonNodeArgument.BooleanTrueValue, null),
                        Arguments.of(JsonNodeArgument.BooleanFalseValue, null),
                        Arguments.of(JsonNodeArgument.BooleanTrueIntValue,
                                JsonNodeArgument.JsonNodeArgumentConstants.BOOLEAN_TRUE_INT),
                        Arguments.of(JsonNodeArgument.ZeroDoubleValue, null),
                        Arguments.of(JsonNodeArgument.DoubleValue, null),
                        Arguments.of(JsonNodeArgument.NegativeDoubleValue, null),
                        Arguments.of(JsonNodeArgument.IntDoubledValue, null),
                        Arguments.of(JsonNodeArgument.NegativeIntDoubledValue, null),
                        Arguments.of(JsonNodeArgument.StringNullValue, null),
                        Arguments.of(JsonNodeArgument.StringEmptyNodeValue, null),
                        Arguments.of(JsonNodeArgument.StringArrayNodeValue, null),
                        Arguments.of(JsonNodeArgument.StringZeroIntValue, null),
                        Arguments.of(JsonNodeArgument.StringIntValue, null),
                        Arguments.of(JsonNodeArgument.StringNegativeIntValue, null),
                        Arguments.of(JsonNodeArgument.StringBooleanTrueValue, null),
                        Arguments.of(JsonNodeArgument.StringBooleanFalseValue, null),
                        Arguments.of(JsonNodeArgument.StringBooleanTrueIntValue, null),
                        Arguments.of(JsonNodeArgument.StringZeroDoubleValue, null),
                        Arguments.of(JsonNodeArgument.StringDoubleValue, null),
                        Arguments.of(JsonNodeArgument.StringNegativeDoubleValue, null),
                        Arguments.of(JsonNodeArgument.StringIntDoubledValue, null),
                        Arguments.of(JsonNodeArgument.StringNegativeIntDoubledValue, null),
                        Arguments.of(JsonNodeArgument.StringBlankValue, null),
                        Arguments.of(JsonNodeArgument.StringValue, null),
                        Arguments.of(JsonNodeArgument.NestedNodeValue, null));
            }
        }

        @Nested
        @TestInstance(TestInstance.Lifecycle.PER_CLASS)
        @DisplayName("Test 'key' parameter in 'getInt(JsonNode node, String key)' method")
        class TestKeyParameter extends AbstractTestIndependentParameter {

            @Override
            protected Object callTestedMethod(final Object arg) {
                return JacksonJsonHelper.getInt(KeyArgument.KeyArgumentConstants.NODE_WITH_INT_VALUE, (String) arg);
            }

            @Override
            protected Class<? extends TestArgumentEnum> getTestArgumentEnum() {
                return KeyArgument.class;
            }

            @Override
            @DisplayName("Call method with relevant values for this parameter")
            @ParameterizedTest(name = "{index} => Test getInt('{'\"key\":4'}', {0}), expecting {1}")
            @MethodSource("getTestArgumentsProvider")
            protected void callTest(Object arg, Object expectedResult) {
                super.test(arg, expectedResult);
            }

            @Override
            protected Stream<Arguments> getTestArguments() {
                return Stream.of(Arguments.of(KeyArgument.Null, null),
                        Arguments.of(KeyArgument.Empty, null),
                        Arguments.of(KeyArgument.Blank, null),
                        Arguments.of(KeyArgument.String, KeyArgument.KeyArgumentConstants.INT_VALUE));
            }
        }
    }

    @Nested
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    @DisplayName("Test 'getString(JsonNode node, String key)' method")
    class GetStringTest {

        @Nested
        @TestInstance(TestInstance.Lifecycle.PER_CLASS)
        @DisplayName("Test 'node' parameter in 'getString(JsonNode node, String key)' method")
        class TestNodeParameter extends AbstractTestIndependentParameter {

            @Override
            protected Object callTestedMethod(final Object arg) {
                return JacksonJsonHelper.getString((JsonNode) arg, JsonNodeArgument.JsonNodeArgumentConstants.KEY);
            }

            @Override
            protected Class<? extends TestArgumentEnum> getTestArgumentEnum() {
                return JsonNodeArgument.class;
            }

            @Override
            @DisplayName("Call method with relevant values for this parameter")
            @ParameterizedTest(name = "{index} => Test getString({0}, \"key\"), expecting {1}")
            @MethodSource("getTestArgumentsProvider")
            protected void callTest(Object arg, Object expectedResult) {
                super.test(arg, expectedResult);
            }

            @Override
            protected Stream<Arguments> getTestArguments() {
                return Stream.of(Arguments.of(JsonNodeArgument.Null, null),
                        Arguments.of(JsonNodeArgument.NullNode, null),
                        Arguments.of(JsonNodeArgument.EmptyNode, null),
                        Arguments.of(JsonNodeArgument.ArrayNode, null),
                        Arguments.of(JsonNodeArgument.StringJsonNodeTypeNullValue,
                                JsonNodeArgument.JsonNodeArgumentConstants.STRING_JSON_NODE_TYPE_NULL),
                        Arguments.of(JsonNodeArgument.EmptyValue, JsonNodeArgument.JsonNodeArgumentConstants.EMPTY),
                        Arguments.of(JsonNodeArgument.EmptyNodeValue, null),
                        Arguments.of(JsonNodeArgument.ArrayNodeValue, null),
                        Arguments.of(JsonNodeArgument.NullNodeValue, null),
                        Arguments.of(JsonNodeArgument.ZeroIntValue, null),
                        Arguments.of(JsonNodeArgument.IntValue, null),
                        Arguments.of(JsonNodeArgument.NegativeIntValue, null),
                        Arguments.of(JsonNodeArgument.BooleanTrueValue, null),
                        Arguments.of(JsonNodeArgument.BooleanFalseValue, null),
                        Arguments.of(JsonNodeArgument.BooleanTrueIntValue, null),
                        Arguments.of(JsonNodeArgument.ZeroDoubleValue, null),
                        Arguments.of(JsonNodeArgument.DoubleValue, null),
                        Arguments.of(JsonNodeArgument.NegativeDoubleValue, null),
                        Arguments.of(JsonNodeArgument.IntDoubledValue, null),
                        Arguments.of(JsonNodeArgument.NegativeIntDoubledValue, null),
                        Arguments.of(JsonNodeArgument.StringNullValue,
                                JsonNodeArgument.JsonNodeArgumentConstants.STRING_NULL),
                        Arguments.of(JsonNodeArgument.StringEmptyNodeValue,
                                JsonNodeArgument.JsonNodeArgumentConstants.STRING_EMPTY_NODE),
                        Arguments.of(JsonNodeArgument.StringArrayNodeValue,
                                JsonNodeArgument.JsonNodeArgumentConstants.STRING_ARRAY_NODE),
                        Arguments.of(JsonNodeArgument.StringZeroIntValue,
                                JsonNodeArgument.JsonNodeArgumentConstants.STRING_ZERO_INT),
                        Arguments.of(JsonNodeArgument.StringIntValue,
                                JsonNodeArgument.JsonNodeArgumentConstants.STRING_INT),
                        Arguments.of(JsonNodeArgument.StringNegativeIntValue,
                                JsonNodeArgument.JsonNodeArgumentConstants.STRING_NEGATIVE_INT),
                        Arguments.of(JsonNodeArgument.StringBooleanTrueValue,
                                JsonNodeArgument.JsonNodeArgumentConstants.STRING_BOOLEAN_TRUE),
                        Arguments.of(JsonNodeArgument.StringBooleanFalseValue,
                                JsonNodeArgument.JsonNodeArgumentConstants.STRING_BOOLEAN_FALSE),
                        Arguments.of(JsonNodeArgument.StringBooleanTrueIntValue,
                                JsonNodeArgument.JsonNodeArgumentConstants.STRING_BOOLEAN_TRUE_INT),
                        Arguments.of(JsonNodeArgument.StringZeroDoubleValue,
                                JsonNodeArgument.JsonNodeArgumentConstants.STRING_ZERO_DOUBLE),
                        Arguments.of(JsonNodeArgument.StringDoubleValue,
                                JsonNodeArgument.JsonNodeArgumentConstants.STRING_DOUBLE),
                        Arguments.of(JsonNodeArgument.StringNegativeDoubleValue,
                                JsonNodeArgument.JsonNodeArgumentConstants.STRING_NEGATIVE_DOUBLE),
                        Arguments.of(JsonNodeArgument.StringIntDoubledValue,
                                JsonNodeArgument.JsonNodeArgumentConstants.STRING_INT_DOUBLED),
                        Arguments.of(JsonNodeArgument.StringNegativeIntDoubledValue,
                                JsonNodeArgument.JsonNodeArgumentConstants.STRING_NEGATIVE_INT_DOUBLED),
                        Arguments.of(JsonNodeArgument.StringBlankValue,
                                JsonNodeArgument.JsonNodeArgumentConstants.STRING_BLANK),
                        Arguments.of(JsonNodeArgument.StringValue, JsonNodeArgument.JsonNodeArgumentConstants.STRING),
                        Arguments.of(JsonNodeArgument.NestedNodeValue, null));
            }
        }

        @Nested
        @TestInstance(TestInstance.Lifecycle.PER_CLASS)
        @DisplayName("Test 'key' parameter in 'getString(JsonNode node, String key)' method")
        class TestKeyParameter extends AbstractTestIndependentParameter {

            @Override
            protected Object callTestedMethod(final Object arg) {
                return JacksonJsonHelper.getString(KeyArgument.KeyArgumentConstants.NODE_WITH_STRING_VALUE,
                        (String) arg);
            }

            @Override
            protected Class<? extends TestArgumentEnum> getTestArgumentEnum() {
                return KeyArgument.class;
            }

            @Override
            @DisplayName("Call method with relevant values for this parameter")
            @ParameterizedTest(name = "{index} => Test getString('{'\"key\":\"value\"'}', {0}), expecting {1}")
            @MethodSource("getTestArgumentsProvider")
            protected void callTest(Object arg, Object expectedResult) {
                super.test(arg, expectedResult);
            }

            @Override
            protected Stream<Arguments> getTestArguments() {
                return Stream.of(Arguments.of(KeyArgument.Null, null),
                        Arguments.of(KeyArgument.Empty, null),
                        Arguments.of(KeyArgument.Blank, null),
                        Arguments.of(KeyArgument.String, KeyArgument.KeyArgumentConstants.STRING_VALUE));
            }
        }
    }

    @Nested
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    @DisplayName("Test 'getJsonNode(JsonNode node, String key)' method")
    class GetJsonNodeTest {

        @Nested
        @TestInstance(TestInstance.Lifecycle.PER_CLASS)
        @DisplayName("Test 'node' parameter in 'getJsonNode(JsonNode node, String key)' method")
        class TestNodeParameter extends AbstractTestIndependentParameter {

            @Override
            protected Object callTestedMethod(final Object arg) {
                return JacksonJsonHelper.getJsonNode((JsonNode) arg, JsonNodeArgument.JsonNodeArgumentConstants.KEY);
            }

            @Override
            protected Class<? extends TestArgumentEnum> getTestArgumentEnum() {
                return JsonNodeArgument.class;
            }

            @Override
            @DisplayName("Call method with relevant values for this parameter")
            @ParameterizedTest(name = "{index} => Test getJsonNode({0}, \"key\"), expecting {1}")
            @MethodSource("getTestArgumentsProvider")
            protected void callTest(Object arg, Object expectedResult) {
                super.test(arg, expectedResult);
            }

            @Override
            protected Stream<Arguments> getTestArguments() {
                return Stream.of(Arguments.of(JsonNodeArgument.Null, null),
                        Arguments.of(JsonNodeArgument.NullNode, null),
                        Arguments.of(JsonNodeArgument.EmptyNode, null),
                        Arguments.of(JsonNodeArgument.ArrayNode, null),
                        Arguments.of(JsonNodeArgument.StringJsonNodeTypeNullValue, null),
                        Arguments.of(JsonNodeArgument.EmptyValue, null),
                        Arguments.of(JsonNodeArgument.EmptyNodeValue,
                                JsonNodeArgument.JsonNodeArgumentConstants.EMPTY_NODE),
                        Arguments.of(JsonNodeArgument.ArrayNodeValue, null),
                        Arguments.of(JsonNodeArgument.NullNodeValue, null),
                        Arguments.of(JsonNodeArgument.ZeroIntValue, null),
                        Arguments.of(JsonNodeArgument.IntValue, null),
                        Arguments.of(JsonNodeArgument.NegativeIntValue, null),
                        Arguments.of(JsonNodeArgument.BooleanTrueValue, null),
                        Arguments.of(JsonNodeArgument.BooleanFalseValue, null),
                        Arguments.of(JsonNodeArgument.BooleanTrueIntValue, null),
                        Arguments.of(JsonNodeArgument.ZeroDoubleValue, null),
                        Arguments.of(JsonNodeArgument.DoubleValue, null),
                        Arguments.of(JsonNodeArgument.NegativeDoubleValue, null),
                        Arguments.of(JsonNodeArgument.IntDoubledValue, null),
                        Arguments.of(JsonNodeArgument.NegativeIntDoubledValue, null),
                        Arguments.of(JsonNodeArgument.StringNullValue, null),
                        Arguments.of(JsonNodeArgument.StringEmptyNodeValue, null),
                        Arguments.of(JsonNodeArgument.StringArrayNodeValue, null),
                        Arguments.of(JsonNodeArgument.StringZeroIntValue, null),
                        Arguments.of(JsonNodeArgument.StringIntValue, null),
                        Arguments.of(JsonNodeArgument.StringNegativeIntValue, null),
                        Arguments.of(JsonNodeArgument.StringBooleanTrueValue, null),
                        Arguments.of(JsonNodeArgument.StringBooleanFalseValue, null),
                        Arguments.of(JsonNodeArgument.StringBooleanTrueIntValue, null),
                        Arguments.of(JsonNodeArgument.StringZeroDoubleValue, null),
                        Arguments.of(JsonNodeArgument.StringDoubleValue, null),
                        Arguments.of(JsonNodeArgument.StringNegativeDoubleValue, null),
                        Arguments.of(JsonNodeArgument.StringIntDoubledValue, null),
                        Arguments.of(JsonNodeArgument.StringNegativeIntDoubledValue, null),
                        Arguments.of(JsonNodeArgument.StringBlankValue, null),
                        Arguments.of(JsonNodeArgument.StringValue, null),
                        Arguments.of(JsonNodeArgument.NestedNodeValue,
                                JsonNodeArgument.JsonNodeArgumentConstants.STRING_VALUE));
            }
        }

        @Nested
        @TestInstance(TestInstance.Lifecycle.PER_CLASS)
        @DisplayName("Test 'key' parameter in 'getJsonNode(JsonNode node, String key)' method")
        class TestKeyParameter extends AbstractTestIndependentParameter {

            @Override
            protected Object callTestedMethod(final Object arg) {
                return JacksonJsonHelper.getJsonNode(KeyArgument.KeyArgumentConstants.NODE_WITH_NODE_VALUE,
                        (String) arg);
            }

            @Override
            protected Class<? extends TestArgumentEnum> getTestArgumentEnum() {
                return KeyArgument.class;
            }

            @Override
            @DisplayName("Call method with relevant values for this parameter")
            @ParameterizedTest(name = "{index} => Test getJsonNode('{'\"key\":'{\"key\":4'}''}', {0}), expecting {1}")
            @MethodSource("getTestArgumentsProvider")
            protected void callTest(Object arg, Object expectedResult) {
                super.test(arg, expectedResult);
            }

            @Override
            protected Stream<Arguments> getTestArguments() {
                return Stream.of(Arguments.of(KeyArgument.Null, null),
                        Arguments.of(KeyArgument.Empty, null),
                        Arguments.of(KeyArgument.Blank, null),
                        Arguments.of(KeyArgument.String, KeyArgument.KeyArgumentConstants.NODE_VALUE));
            }
        }
    }

    @Nested
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    @DisplayName("Test 'readJsonTreeFromString(String)' method")
    class ReadJsonTreeFromStringTest {

        @Nested
        @TestInstance(TestInstance.Lifecycle.PER_CLASS)
        @DisplayName("Test 'stringTree' parameter in 'readJsonTreeFromString(String)' method")
        class TestStringTreeParameter extends AbstractTestIndependentParameter {

            @Override
            @DisplayName("Call method with relevant values for this parameter")
            @ParameterizedTest(name = "{index} => Test readJsonTreeFromString({0}), expecting {1}")
            @MethodSource("getTestArgumentsProvider")
            protected void callTest(final Object arg, final Object expectedResult) {
                super.test(arg, expectedResult);
            }

            @Override
            protected Stream<Arguments> getTestArguments() {
                return Stream.of(Arguments.of(StringJsonTreeArgument.Null, IllegalArgumentException.class),
                        Arguments.of(StringJsonTreeArgument.Empty, MissingNode.getInstance()),
                        Arguments.of(StringJsonTreeArgument.Blank, MissingNode.getInstance()),
                        Arguments.of(StringJsonTreeArgument.String, JsonParseException.class),
                        Arguments.of(StringJsonTreeArgument.EmptyArray,
                                StringJsonTreeArgument.StringJsonTreeArgumentConstants.EMPTY_ARRAY_NODE),
                        Arguments.of(StringJsonTreeArgument.IntArray,
                                StringJsonTreeArgument.StringJsonTreeArgumentConstants.INT_ARRAY_NODE),
                        Arguments.of(StringJsonTreeArgument.NodeArray,
                                StringJsonTreeArgument.StringJsonTreeArgumentConstants.NODE_ARRAY_NODE),
                        Arguments.of(StringJsonTreeArgument.EmptyNode,
                                StringJsonTreeArgument.StringJsonTreeArgumentConstants.EMPTY_NODE_NODE),
                        Arguments.of(StringJsonTreeArgument.NodeWithArray,
                                StringJsonTreeArgument.StringJsonTreeArgumentConstants.NODE_WITH_ARRAY_NODE),
                        Arguments.of(StringJsonTreeArgument.ArrayWithDifferentTypes,
                                StringJsonTreeArgument.StringJsonTreeArgumentConstants.ARRAY_WITH_DIFFERENT_TYPES_NODE));
            }

            @Override
            protected Object callTestedMethod(final Object arg) throws TestException {
                try {
                    return JacksonJsonHelper.readJsonTreeFromString((String) arg);
                } catch (final Exception e) {
                    throw new TestException(e);
                }
            }

            @Override
            protected Class<? extends TestArgumentEnum> getTestArgumentEnum() {
                return StringJsonTreeArgument.class;
            }
        }
    }

    @Nested
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    @DisplayName("Test 'isNodeNull(JsonNode node)' method")
    class IsNodeNullTest {

        Method isNodeNull = JacksonJsonHelper.class.getDeclaredMethod("isNodeNull", JsonNode.class);

        /**
         * @throws NoSuchMethodException if isNodeNull method is not found in JacksonJsonHelper class
         */
        IsNodeNullTest() throws NoSuchMethodException {
            isNodeNull.setAccessible(true);
        }

        @Nested
        @TestInstance(TestInstance.Lifecycle.PER_CLASS)
        @DisplayName("Test 'node' parameter in 'isNodeNull(JsonNode node)' method")
        class TestNodeParameter extends AbstractTestIndependentParameter {

            @Override
            @DisplayName("Call method with relevant values for this parameter")
            @ParameterizedTest(name = "{index} => Test isNodeNull({0}), expecting {1}")
            @MethodSource("getTestArgumentsProvider")
            protected void callTest(final Object arg, final Object expectedResult) {
                super.test(arg, expectedResult);
            }

            @Override
            protected Stream<Arguments> getTestArguments() {
                return Stream.of(Arguments.of(JsonNodeArgument.Null, true),
                        Arguments.of(JsonNodeArgument.NullNode, true),
                        Arguments.of(JsonNodeArgument.EmptyNode, false),
                        Arguments.of(JsonNodeArgument.ArrayNode, false),
                        Arguments.of(JsonNodeArgument.StringJsonNodeTypeNullValue, false),
                        Arguments.of(JsonNodeArgument.EmptyValue, false),
                        Arguments.of(JsonNodeArgument.EmptyNodeValue, false),
                        Arguments.of(JsonNodeArgument.ArrayNodeValue, false),
                        Arguments.of(JsonNodeArgument.NullNodeValue, false),
                        Arguments.of(JsonNodeArgument.ZeroIntValue, false),
                        Arguments.of(JsonNodeArgument.IntValue, false),
                        Arguments.of(JsonNodeArgument.NegativeIntValue, false),
                        Arguments.of(JsonNodeArgument.BooleanTrueValue, false),
                        Arguments.of(JsonNodeArgument.BooleanFalseValue, false),
                        Arguments.of(JsonNodeArgument.BooleanTrueIntValue, false),
                        Arguments.of(JsonNodeArgument.ZeroDoubleValue, false),
                        Arguments.of(JsonNodeArgument.DoubleValue, false),
                        Arguments.of(JsonNodeArgument.NegativeDoubleValue, false),
                        Arguments.of(JsonNodeArgument.IntDoubledValue, false),
                        Arguments.of(JsonNodeArgument.NegativeIntDoubledValue, false),
                        Arguments.of(JsonNodeArgument.StringNullValue, false),
                        Arguments.of(JsonNodeArgument.StringEmptyNodeValue, false),
                        Arguments.of(JsonNodeArgument.StringArrayNodeValue, false),
                        Arguments.of(JsonNodeArgument.StringZeroIntValue, false),
                        Arguments.of(JsonNodeArgument.StringIntValue, false),
                        Arguments.of(JsonNodeArgument.StringNegativeIntValue, false),
                        Arguments.of(JsonNodeArgument.StringBooleanTrueValue, false),
                        Arguments.of(JsonNodeArgument.StringBooleanFalseValue, false),
                        Arguments.of(JsonNodeArgument.StringBooleanTrueIntValue, false),
                        Arguments.of(JsonNodeArgument.StringZeroDoubleValue, false),
                        Arguments.of(JsonNodeArgument.StringDoubleValue, false),
                        Arguments.of(JsonNodeArgument.StringNegativeDoubleValue, false),
                        Arguments.of(JsonNodeArgument.StringIntDoubledValue, false),
                        Arguments.of(JsonNodeArgument.StringNegativeIntDoubledValue, false),
                        Arguments.of(JsonNodeArgument.StringBlankValue, false),
                        Arguments.of(JsonNodeArgument.StringValue, false),
                        Arguments.of(JsonNodeArgument.NestedNodeValue, false));
            }

            @Override
            protected Object callTestedMethod(final Object arg) throws TestException {
                try {
                    return isNodeNull.invoke(null, arg);
                } catch (final IllegalAccessException | InvocationTargetException e) {
                    throw new TestException(e);
                }
            }

            @Override
            protected Class<? extends TestArgumentEnum> getTestArgumentEnum() {
                return JsonNodeArgument.class;
            }
        }
    }
}